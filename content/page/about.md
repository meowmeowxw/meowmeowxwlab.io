---
title: About
subtitle: meowmeowxw
comments: false
---

I'm Giovanni, a computer science student and member of the 
[CeSeNA](https://cesena.github.io) team.

My main interests are cryptography, binary exploitation and reverse engineering.

PGP Fingerprint: `9FD8 8BF7 B5D9 1730 5FCC  603E 80DC 7971 20B4 900B`

[PGP Key](https://api.protonmail.ch/pks/lookup?op=get&search=giovanni.disanti@protonmail.com)

