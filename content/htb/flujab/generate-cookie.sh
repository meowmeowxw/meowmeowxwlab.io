#!/bin/bash
ip=$(ip addr show tun0 | awk 'NR==3' | awk '{print $2}');
ip=${ip::-3}
haship=$(echo -n $ip | md5sum | awk '{print $1}');
echo "Patient:$haship"
modus=$(echo -n "Configure=True" | base64)
echo "Modus:$modus"
registered=$(echo -n "$haship=True" | base64)
echo "Registered:$registered"
