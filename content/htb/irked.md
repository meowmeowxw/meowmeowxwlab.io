---
title: "Hack The Box - Irked"
date: 2018-12-19
tags:
  - htb
---

Exploit Unrealircd vulnerability with msf to get a user shell & exploit local
suid binary to get a root shell.

## Description

- Name: `Irked`
- IP: `10.10.10.117`
- Author: `MrAgent`
- Difficulty: `4/10`

## Discovery

`sudo nmap -sV -sC -sS -A -v -p- irked.htb -oA nmap`

```
PORT      STATE SERVICE VERSION
22/tcp    open  ssh     OpenSSH 6.7p1 Debian 5+deb8u4 (protocol 2.0)
| ssh-hostkey:
|   1024 6a:5d:f5:bd:cf:83:78:b6:75:31:9b:dc:79:c5:fd:ad (DSA)
|   2048 75:2e:66:bf:b9:3c:cc:f7:7e:84:8a:8b:f0:81:02:33 (RSA)
|   256 c8:a3:a2:5e:34:9a:c4:9b:90:53:f7:50:bf:ea:25:3b (ECDSA)
|_  256 8d:1b:43:c7:d0:1a:4c:05:cf:82:ed:c1:01:63:a2:0c (EdDSA)
80/tcp    open  http    Apache httpd 2.4.10 ((Debian))
| http-methods:
|_  Supported Methods: POST OPTIONS GET HEAD
|_http-server-header: Apache/2.4.10 (Debian)
|_http-title: Site doesn't have a title (text/html).
111/tcp   open  rpcbind 2-4 (RPC #100000)
| rpcinfo:
|   program version   port/proto  service
|   100000  2,3,4        111/tcp  rpcbind
|   100000  2,3,4        111/udp  rpcbind
|   100024  1          50944/udp  status
|_  100024  1          52118/tcp  status
6697/tcp  open  irc     UnrealIRCd
8067/tcp  open  irc     UnrealIRCd
52118/tcp open  status  1 (RPC #100024)
65534/tcp open  irc     UnrealIRCd

```

## Pwn

This is the index page of the web site

![](./web.png)

Trying to use gobuster to enumerate the directories can't find anything useful, so we can move to search other exploits for the port `6697` and `8067` (*irc*).

`searchsploit unrealircd`

```
------------------------------------------------------------- ----------------------------------
 Exploit Title                                               |  Path
                                                             | (/opt/exploitdb/)
------------------------------------------------------------- ----------------------------------
UnrealIRCd 3.2.8.1 - Backdoor Command Execution (Metasploit) | exploits/linux/remote/16922.rb
UnrealIRCd 3.2.8.1 - Local Configuration Stack Overflow      | exploits/windows/dos/18011.txt
UnrealIRCd 3.2.8.1 - Remote Downloader/Execute               | exploits/linux/remote/13853.pl
UnrealIRCd 3.x - Remote Denial of Service                    | exploits/windows/dos/27407.pl
------------------------------------------------------------- ----------------------------------

```

The one available on metasploit is the `UnrealIRCd 3.2.8.1`.

![](./metasploit.png)

The exploit works and we get a shell.

![](./user-escalation.png)

The user flag is in the djmardov directory, but we can see the flag only if we are djmardov.
The word `steg` in the `.backup` file suggests us that djmardov uses some kind of steganography to encode his password.
Using `steghide` on the web's image, and using as password : `UPupDOWNdownLR1rBAbaSSss` gives us the password for `djmardov`.

![](./steghide.png)

In the victim's machine, we can login with

```sh
ircd@irked:/home/djmardov/Documents$ su - djmardov
djmardov@irked:~$ cd Documents
cd Documents
djmardov@irked:~/Documents$ cat user.txt
cat user.txt
4a66a78b12dc0e661a59d3f5c0267a8e
```

We can use the `find` command  to see what files have the setuid permission set:

(4000 -> setuid),(2000 -> setgid),(1000 -> sticky bit).

![](./find.png)

The program that caught my attention is `viewuser`.

If we try to execute `viewuser` it gives us the following output

```
viewuser
This application is being devleoped to set and test user permissions
It is still being actively developed
(unknown) :0           2018-12-19 15:14 (:0)
djmardov pts/0        2018-12-19 15:15 (10.10.12.35)
djmardov pts/1        2018-12-19 15:15 (10.10.15.82)
djmardov pts/2        2018-12-19 15:15 (10.10.13.138)
djmardov pts/4        2018-12-19 15:16 (10.10.15.233)
djmardov pts/5        2018-12-19 15:16 (10.10.16.28)
djmardov pts/8        2018-12-19 15:17 (10.10.15.147)
djmardov pts/10       2018-12-19 15:22 (10.10.12.115)
djmardov pts/11       2018-12-19 15:23 (10.10.19.221)
sh: 1: /tmp/listusers: Permission denied
```

We can see that there's some error on `/tmp/listusers`. If we go in the `/tmp` directory we can change the permission of the `listusers` file and the contents of it adding to the end's file `cat /root/root.txt` or `/bin/bash`

![](./root.png)

And we get ROOT shell.
