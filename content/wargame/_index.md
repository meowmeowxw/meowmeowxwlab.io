# Wargame

In hacking, a wargame (or war game) is a cyber-security challenge and mind sport 
in which the competitors must exploit or defend a vulnerability in a 
system or application, or gain or prevent access to a computer system.

## [Narnia](./narnia/)

This wargame is for the ones that want to learn basic exploitation. 
You can see the most common bugs in this game and we've tried to make them
easy to exploit.

## [ROP Emporium](./rop-emporium)

Learn return-oriented programming through a series of challenges designed 
to teach ROP techniques in isolation, with minimal reverse-engineering and
bug-hunting.


## [Leet Time SQL Ninja](./leet-sqlninja)

SQL Injection Ninja Lab is a lab which provides a complete testing environment
for anyone who is interested to learn SQL injection or sharpen his Injecting
skills.

