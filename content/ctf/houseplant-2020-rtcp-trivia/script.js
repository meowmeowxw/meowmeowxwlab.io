console.log("loaded successful");
Java.perform(function x() {
    Java.deoptimizeEverything()
    var json;
    var done = 1;
    var b64 = Java.use('android.util.Base64');
    var stringJava = Java.use('java.lang.String');
    var nwClass = Java.use('nw');
    var jsonObject = Java.use('org.json.JSONObject');
    jsonObject.$init.overload('java.lang.String').implementation = function(s) {
        console.log("calling jsonObject, with arg:" + s);
        // duplicate this object for later use outside the overload
        json = Java.retain(this);
        return this.$init(s);
    }

    var cipher = Java.use('javax.crypto.Cipher');
    cipher.init.overload('int', 'java.security.Key', 'java.security.spec.AlgorithmParameterSpec').implementation = function(a1, a2, a3) {
        console.log("calling cipher, with arg:");
        var a = this.init(a1, a2, a3);
        console.log("a");
        var dec = this.doFinal(b64.decode(json.getString("correctAnswer"), 0));
        var answer = stringJava.$new(dec);
        console.log("done: " + done);
        done = done + 1;
        nwClass.a().a("{\"method\":\"answer\",\"answer\":" + answer + "}");
        return a;
    };
});

