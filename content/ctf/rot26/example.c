#include <stdio.h>

int sum(int x, int y)
{
	return x + y;
}
int main()
{
	int x = 10;
	int y = 20;
	printf("%2$d and %1$d", x, y);
	printf("%08x %08x %08x %08x %08x\n");
	printf("address of sum : %p", &sum); 
	return 0;
}
