#!/usr/bin/env sage
def main():
    x_0 = 2
    y_0 = 5398141
    x_1 = 3
    y_1 = 5398288
    x_2 = 5
    y_2 = 5398756
    R.<x> = QQ[]
    l_0 = ((x - x_1) / (x_0 - x_1)) * ((x - x_2) / (x_0 - x_2))
    l_1 = ((x - x_0) / (x_1 - x_0)) * ((x - x_2) / (x_1 - x_2))
    l_2 = ((x - x_0) / (x_2 - x_0)) * ((x - x_1) / (x_2 - x_1))
    f_x = (y_0 * l_0) + (y_1 * l_1) + (y_2 * l_2)
    print(f_x)

if __name__ == '__main__':
    main()

