---
title: "Redpwn 2019 - Tux Trivia Show"
date: 2019-08-16
tags:
  - ctf
  - misc
  - redpwn
---

Misc challenge on capitals, with @edoz90.

## Information

- _category_: misc
- _points_: 50

## Description

> Win Tux Trivia Show!

> nc chall2.2019.redpwn.net 6001

## Writeup

The challenge is a classic misc where you are asked to respond to a series of questions. This time is about world capitals.

![](./serbia.png)

We wrote a python script to interact with the server and find the correct answer.

```python
from pwn import log, remote
import requests

capitals = {
    "Afghanistan": "Kabul",
    "Alabama": "Montgomery",
    "Alaska": "Juneau",
    "Albania": "Tirana",
    "Algeria": "Algiers",
    "Andorra": "Andorra la Vella",
    "Angola": "Luanda",
    "Argentina": "Buenos Aires",
    "Arizona": "Phoenix",
    "Arkansas": "Little Rock",
    "Armenia": "Yerevan",
    "Australia": "Canberra",
    "Austria": "Vienna",
    "Azerbaijan": "Baku",
    "Bahamas": "Nassau",
    "Bahrain": "Manama",
    "Bangladesh": "Dhaka",
    "Barbados": "Bridgetown",
    "Belarus": "Minsk",
    "Belize": "Belmopan",
    "Bhutan": "Thimphu",
    "Bosnia and Herzegovina": "Sarajevo",
    "Botswana": "Gaborone",
    "Brunei": "Bandar Seri Begawan",
    "Bulgaria": "Sofia",
    "Burkina Faso": "Ouagadougou",
    "California": "Sacramento",
    "Cambodia": "Phnom Penh",
    "Canada": "Ottawa",
    "Cape Verde": "Praia",
    "Central African Republic": "Bangui",
    "Chad": "N'Djamena",
    "Colorado": "Denver",
    "Comoros": "Moroni",
    "Connecticut": "Hartford",
    "Croatia": "Zagreb",
    "Cyprus": "Nicosia",
    "Czech Republic": "Prague",
    "Delaware": "Dover",
    "Democratic Republic of the Congo": "Kinshasa",
    "Denmark": "Copenhagen",
    "Djibouti": "Djibouti",
    "Dominica": "Roseau",
    "Dominican Republic": "Santo Domingo",
    "East Timor (Timor-Leste)": "Dili",
    "Egypt": "Cairo",
    "El Salvador": "San Salvador",
    "Equatorial Guinea": "Malabo",
    "Eritrea": "Asmara",
    "Estonia": "Tallinn",
    "Ethiopia": "Addis Ababa",
    "Federated States of Micronesia": "Palikir",
    "Fiji": "Suva",
    "Finland": "Helsinki",
    "Florida": "Tallahassee",
    "France": "Paris",
    "Gabon": "Libreville",
    "Gambia": "Banjul",
    "Georgia": "Atlanta",
    "Germany": "Berlin",
    "Ghana": "Accra",
    "Greece": "Athens",
    "Guatemala": "Guatemala City",
    "Guinea": "Conakry",
    "Guinea-Bissau": "Bissau",
    "Guyana": "Georgetown",
    "Haiti": "Port-au-Prince",
    "Hawaii": "Honolulu",
    "Honduras": "Tegucigalpa",
    "Hungary": "Budapest",
    "Idaho": "Boise",
    "Illinois": "Springfield",
    "India": "New Delhi",
    "Indiana": "Indianapolis",
    "Indonesia": "Jakarta",
    "Iowa": "Des Moines",
    "Iran": "Tehran",
    "Iraq": "Baghdad",
    "Ireland": "Dublin",
    "Italy": "Rome",
    "Jamaica": "Kingston",
    "Japan": "Tokyo",
    "Jordan": "Amman",
    "Kansas": "Topeka",
    "Kazakhstan": "Astana",
    "Kentucky": "Frankfort",
    "Kenya": "Nairobi",
    "Kiribati": "Tarawa",
    "Kuwait": "Kuwait City",
    "Kyrgyzstan": "Bishkek",
    "Laos": "Vientiane",
    "Latvia": "Riga",
    "Lebanon": "Beirut",
    "Lesotho": "Maseru",
    "Liberia": "Monrovia",
    "Libya": "Tripoli",
    "Liechtenstein": "Vaduz",
    "Lithuania": "Vilnius",
    "Louisiana": "Baton Rouge",
    "Luxembourg": "Luxembourg",
    "Macedonia": "Skopje",
    "Madagascar": "Antananarivo",
    "Maine": "Augusta",
    "Malawi": "Lilongwe",
    "Mali": "Bamako",
    "Malta": "Valletta",
    "Marshall Islands": "Majuro",
    "Maryland": "Annapolis",
    "Massachusetts": "Boston",
    "Mauritania": "Nouakchott",
    "Mauritius": "Port Louis",
    "Mexico": "Mexico City",
    "Michigan": "Lansing",
    "Minnesota": "St. Paul",
    "Mississippi": "Jackson",
    "Missouri": "Jefferson City",
    "Moldova": "Chisinau",
    "Montana": "Helena",
    "Morocco": "Rabat",
    "Mozambique": "Maputo",
    "Namibia": "Windhoek",
    "Nebraska": "Lincoln",
    "Nepal": "Kathmandu",
    "Nevada": "Carson City",
    "New Hampshire": "Concord",
    "New Jersey": "Trenton",
    "New Mexico": "Santa Fe",
    "New York": "Albany",
    "New Zealand": "Wellington",
    "Nicaragua": "Managua",
    "Niger": "Niamey",
    "Nigeria": "Abuja",
    "North Carolina": "Raleigh",
    "North Dakota": "Bismarck",
    "North Korea": "Pyongyang",
    "Norway": "Oslo",
    "Ohio": "Columbus",
    "Oklahoma": "Oklahoma City",
    "Oman": "Muscat",
    "Oregon": "Salem",
    "Pakistan": "Islamabad",
    "Panama": "Panama City",
    "Papua New Guinea": "Port Moresby",
    "Pennsylvania": "Harrisburg",
    "Peru": "Lima",
    "Philippines": "Manila",
    "Poland": "Warsaw",
    "Portugal": "Lisbon",
    "Qatar": "Doha",
    "Republic of the Congo": "Brazzaville",
    "Rhode Island": "Providence",
    "Romania": "Bucharest",
    "Russia": "Moscow",
    "Rwanda": "Kigali",
    "Saint Kitts and Nevis": "Basseterre",
    "Saint Lucia": "Castries",
    "Saint Vincent and the Grenadines": "Kingstown",
    "Samoa": "Apia",
    "San Marino": "San Marino",
    "Saudi Arabia": "Riyadh",
    "Senegal": "Dakar",
    "Serbia": "Belgrade",
    "Seychelles": "Victoria",
    "Sierra Leone": "Freetown",
    "Slovakia": "Bratislava",
    "Slovenia": "Ljubljana",
    "Solomon Islands": "Honiara",
    "Somalia": "Mogadishu",
    "South Carolina": "Columbia",
    "South Dakota": "Pierre",
    "South Korea": "Seoul",
    "South Sudan": "Juba",
    "Spain": "Madrid",
    "Sudan": "Khartoum",
    "Suriname": "Paramaribo",
    "Sweden": "Stockholm",
    "Switzerland": "Bern",
    "Syria": "Damascus",
    "Tajikistan": "Dushanbe",
    "Tennessee": "Nashville",
    "Texas": "Austin",
    "Thailand": "Bangkok",
    "Tonga": "Nuku'alofa",
    "Trinidad and Tobago": "Port of Spain",
    "Tunisia": "Tunis",
    "Turkey": "Ankara",
    "Turkmenistan": "Ashgabat",
    "Tuvalu": "Funafuti",
    "Uganda": "Kampala",
    "Ukraine": "Kiev",
    "United Arab Emirates": "Abu Dhabi",
    "Uruguay": "Montevideo",
    "Utah": "Salt Lake City",
    "Uzbekistan": "Tashkent",
    "Vanuatu": "Port Vila",
    "Venezuela": "Caracas",
    "Vermont": "Montpelier",
    "Vietnam": "Hanoi",
    "Virginia": "Richmond",
    "Washington": "Olympia",
    "West Virginia": "Charleston",
    "Wisconsin": "Madison",
    "Wyoming": "Cheyenne",
    "Yemen": "Sana'a",
    "Zambia": "Lusaka",
    "Zimbabwe": "Harare",
}

conn = remote("chall.2019.redpwn.net", "6001")
log.info(conn.recvline())
count = 0
while True:
    line = ''
    while True:
        try:
            line += conn.recv(1)
            if line[-1:] == '\n':
                break
        except EOFError:
            break

    log.info(line)

    if "What is the capital of " in line:
        line = line.strip()
        res = line.replace("What is the capital of ", "")[:-1]
        count += 1
        if res in capitals:
            conn.sendline(capitals.get(res))
            print(capitals.get(res))
        else:
            response = requests.get("https://restcountries.eu/rest/v2/name/" + res)
            data = response.json()
            capital = data[0]["capital"]
            conn.sendline(capital)
            print(capital)
        log.info(conn.recvline())
        log.info(conn.recvline())
    else:
        break

log.warning("count: " + str(count))
```

After 1000 questions we got the flag.

![](./flag.png)

## Flag

`flag{TUX_tr1v1A_sh0w+m3st3r3d_:D}`
