---
title: "Redpwn 2019 - Super Hash"
date: 2019-08-16
tags:
 - ctf
 - crypto
 - redpwn
---

Sequencial hashing, with @edoz90.

## Information

*   *category*: crypto
*   *points*: 50

## Description

> Does hashing something multiple times make it more secure? I sure hope so. I've hashed my secret ten times with md5! Hopefully this makes up for the fact that my secret is really short. Wrap the secret in flag{}.

> Note: Follow the format of the provided hash exactly

> Hash: CD04302CBBD2E0EB259F53FAC7C57EE2

## Writeup

Because the `secret` is **really** short I supposed that it wasn't longer than 3 characters. 

I created a `charset.lst` :

```
mixalpha-numeric-all-space = [abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRTSUVWXYZ0123456789{};\./,?+=-_\|[]~%^&*()]
```

And I used `crunch` as follow :

`crunch 1 3 -f charset.lst mixalpha-numeric-all-space -o wordlist.txt`

Now I needed to code a python script that computes an hash iteratated 10 times of every word in the wordlist, and compare it with `CD04302CBBD2E0EB259F53FAC7C57EE2`. In the iteration process I have to rehash the hexdigest of the precedent word in uppercase.

`exploit.py` :

```py
from Crypto.Hash import MD5

lines = [line.rstrip('\n') for line in open("./wordlist.txt", "r")] 

for l in lines:
    secret = l
    for i in range(1, 11):
        h = MD5.new()
        h.update(l.encode())
        l = h.digest().hex().upper()
    if l == "CD04302CBBD2E0EB259F53FAC7C57EE2":
        print("found")
        print("secret : " + str(secret))
        break
```

Output :

```
found
secret : ^
```

### Alternative Method

We can also use john :

```sh
$ john hash.txt --format='dynamic=MD5(MD5(MD5(MD5(MD5(MD5(MD5(MD5(MD5(MD5($p))))))))))' --incremental --min-length=1 --max-length=20
```

## Flag

`flag{^}`
