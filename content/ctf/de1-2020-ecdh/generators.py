#!/usr/bin/env python3
# CLIENT

import sys
#import random
from pwn import *
from Crypto.Util.number import bytes_to_long as bl
from factordb.factordb import FactorDB
from Crypto.Util.number import long_to_bytes as lb
from sage.all import *
import string
import subprocess

def crack(HASH):
    alphabet = string.digits + string.ascii_letters
    for x in alphabet:
        for y in alphabet:
            for z in alphabet:
                for w in alphabet:
                    key = x+y+z+w + secret
                    _hash = hashlib.sha256(key.encode()).hexdigest()
                    if _hash == HASH:
                        return x + y + z + w

def xor_string(s1, s2):
    return bytes(x ^ y for x, y in zip(s1, s2))

def pad(m):
    pad_length = 256*2 - len(m)
    for _ in range(pad_length):
        m.insert(0,0)
        return m

def pointToKeys(p):
    x = p[0]
    y = p[1]
    tmp = x << 256 | y
    res = pad([int(i) for i in list('{0:0b}'.format(tmp))]) 
    return res

def bytes_to_bit(a):
    l = []
    for b in a: 
        c = bin(b)[2:].rjust(8, '0') 
        for d in c: 
            l.append(int(d))
    return l

def keysToPoint(k):
    y = int(''.join(str(i) for i in k[-256:]), 2)
    x = int(''.join(str(i) for i in k[:256]), 2)
    return (x, y)

def read_sage_point(P):
    tmp = P.replace('(', '').replace(')', '').split(' : ')
    return (tmp[0], tmp[1])

a = 0x4cee8d95bb3f64db7d53b078ba3a904557425e2a6d91c5dfbf4c564a3f3619fa
b = 0x56cbc73d8d2ad00e22f12b930d1d685136357d692fa705dae25c66bee23157b8
q = 0xdd7860f2c4afe6d96059766ddd2b52f7bb1ab0fce779a36f723d50339ab25bbd

# Solve PoW
"""
conn = remote('134.175.225.42', 8848)
conn.recv(12)
secret = conn.recvuntil(b')')[:-1].decode()
conn.recvuntil(b'== ')
to_find = conn.recvline().strip(b'\n').decode()
print(secret)
print(to_find)
hash = crack(to_find)
conn.recvuntil(b':')
conn.sendline(hash)
"""

context.log_level = 'DEBUG'

generators = {}

for i in range(2, 50):
    if generators.get(i, None) is None:
        E = EllipticCurve(GF(q), [a, i])
        order = E.order()
        f = FactorDB(order)
        f.connect()
        primes = f.get_factor_list()
        valid = 0
        for p in primes:
            if p <= 122985417205330:
                valid = p
        prime = valid
        G = E.gen(0) * int(order / prime)
        G = read_sage_point(str(G))
        generators[i] = (G[0], G[1], prime)
        print(generators)

print(generators)
blacklist_dlog = [30]
"""
pp = []
dlog = []
blacklist = [20, 16, 41, 34, 24, 46, 36, 17, 37, 28]
for i in range(15):

    # Exchange like a beast
    print("\n\nEXCHANGE\n")
    b = randint(10, 50)
    while b in blacklist:
        b = randint(10, 50)
    print(f"b: {b}")
    E = EllipticCurve(GF(q), [a, b])
    print(f"Curve: {E}")
    order = E.order()
    print("computing prime factors")
    f = FactorDB(order)
    f.connect()
    primes = f.get_factor_list()
    #primes = prime_factors(order)
    valid = []
    # 12298541720533
    for p in primes:
        if p <= 122985417205330:
            valid.append(p)
    prime = valid[-1:][0]
    pp.append(prime)
    print(f"prime order: {prime}")
    print("computing generator")
    G = E.gen(0) * int(order / prime)
    G = read_sage_point(str(G))

    if i != 0:
        conn.recvuntil(b'choice:\n')
        conn.sendline(b'Exchange')
    conn.recvuntil(b'X:')
    conn.sendline(str(G[0]))
    conn.recvuntil(b'Y:')
    conn.sendline(str(G[1]))

    # Solve DLOG muthafucka 
    conn.recvuntil(b'Tell me your choice:')
    conn.sendline('Encrypt')
    conn.recvuntil('Give me your message(hex):')
    to_cipher = ""
    to_cipher += 'f1'
    for _ in range(63):
        to_cipher += '41'
    conn.sendline(to_cipher)

    print(conn.recvuntil(b'is:\n'))
    res = conn.recvline().strip(b'\n').decode()
    key = xor_string(bytes.fromhex(to_cipher), bytes.fromhex(res))
    Q = keysToPoint(bytes_to_bit(key))
    print(f"Q: {Q}")
    print(f"G: {G}")
    G_e = E([G[0], G[1]])
    Q_e = E([Q[0], Q[1]])
    print("computing dlog")
    logg = G_e.discrete_log(Q_e)
    dlog.append(logg)
    print(f"dlog: {logg}")
    print(f"done: {i}") 

print(f"dlog: {dlog}")
print(f"primes: {pp}")
super_secret = CRT_list(dlog, pp)
print(super_secret)

conn.interactive()
"""
